CREATE DATABASE retail;
SET datestyle TO 'ISO, DMY';
DROP TABLE IF EXISTS personal_data,
    cards,
    transactions,
    checks,
    sku,
    stores,
    groups_sku,
    date_Of_analysis_formation CASCADE;
CREATE TABLE Personal_data
(
    Customer_ID            SERIAL PRIMARY KEY,
    Customer_Name          VARCHAR NOT NULL
        CONSTRAINT ch_customer_name CHECK (
            Customer_Name SIMILAR TO '[A-ZА-ЯЁ]{1}[a-zа-яё\- ]*'
            ),
    Customer_Surname       VARCHAR NOT NULL
        CONSTRAINT ch_customer_surname CHECK (
            Customer_Surname SIMILAR TO '[A-ZА-ЯЁ]{1}[a-zа-яё\- ]*'
            ),
    Customer_Primary_Email VARCHAR
        CONSTRAINT ch_customer_primary_email CHECK (
            Customer_Primary_Email SIMILAR TO '_+@{1}[a-z0-9]+.{1}[a-z]+'
            ),
    Customer_Primary_Phone VARCHAR(12)
        CONSTRAINT ch_customer_primary_phone CHECK (
            Customer_Primary_Phone SIMILAR TO '[+]{1}7[0-9]{10}'
            )
);
CREATE TABLE Cards
(
    Customer_Card_ID SERIAL PRIMARY KEY,
    Customer_ID      BIGINT NOT NULL
);
CREATE TABLE Transactions
(
    Transaction_ID       SERIAL PRIMARY KEY,
    Customer_Card_ID     BIGINT NOT NULL,
    Transaction_Summ     NUMERIC,
    Transaction_DateTime TIMESTAMP,
    Transaction_Store_ID BIGINT NOT NULL
);
CREATE TABLE Checks
(
    Transaction_ID BIGINT NOT NULL,
    SKU_ID         BIGINT NOT NULL,
    SKU_Amount     NUMERIC,
    SKU_Summ       NUMERIC,
    SKU_Summ_Paid  NUMERIC,
    SKU_Discount   NUMERIC
);
CREATE TABLE SKU
(
    SKU_ID   SERIAL PRIMARY KEY,
    SKU_Name VARCHAR
        CONSTRAINT ch_sku_name CHECK (SKU_Name SIMILAR TO '[A-ZА-ЯЁa-zа-яё\- 0-9]*'),
    Group_ID BIGINT NOT NULL
);
CREATE TABLE Stores
(
    Transaction_Store_ID BIGINT NOT NULL,
    SKU_ID               BIGINT NOT NULL,
    SKU_Purchase_Price   NUMERIC,
    SKU_Retail_Price     NUMERIC
);
CREATE TABLE Groups_SKU
(
    Group_ID   SERIAL PRIMARY KEY,
    Group_Name VARCHAR
        CONSTRAINT ch_group_name CHECK (Group_Name SIMILAR TO '[A-ZА-ЯЁa-zа-яё\- 0-9]*')
);
CREATE TABLE Date_Of_Analysis_Formation
(
    Analysis_Formation TIMESTAMP
);
--

CREATE OR REPLACE PROCEDURE importFromFile(tablename text, pathtofile text, delimiter text)
    LANGUAGE plpgsql AS
$$
BEGIN
    EXECUTE format(
            'COPY %s FROM %L WITH DELIMITER %L CSV;',
            $1,
            $2,
            $3
        );
    IF tablename ILIKE 'personal_data' THEN
        EXECUTE format(
                'SELECT setval(''personal_data_customer_id_seq'', (SELECT MAX(customer_id) FROM %1$s));',
                $1
            );
    ELSEIF tablename ILIKE 'cards' THEN
        EXECUTE format(
                'SELECT setval(''cards_customer_card_id_seq'', (SELECT MAX(customer_card_id) FROM %1$s));',
                $1
            );
    ELSEIF tablename ILIKE 'transactions' THEN
        EXECUTE format(
                'SELECT setval(''transactions_transaction_id_seq'', (SELECT MAX(transaction_id) FROM %1$s));',
                $1
            );
    ELSEIF tablename ILIKE 'sku' THEN
        EXECUTE format(
                'SELECT setval(''sku_sku_id_seq'', (SELECT MAX(sku_id) FROM %1$s));',
                $1
            );
    ELSEIF tablename ILIKE 'groups_sku' THEN
        EXECUTE format(
                'SELECT setval(''groups_sku_group_id_seq'', (SELECT MAX(group_id) FROM %1$s));',
                $1
            );
    END IF;
END;
$$;

CREATE OR REPLACE PROCEDURE exportToFile(tablename text, pathtofile text, delimiter text)
    LANGUAGE plpgsql
AS
$$
BEGIN
    EXECUTE format('COPY %s TO %L DELIMITER %L CSV;', $1, $2, $3);
END;
$$;
-- --WINDOWS
CALL importFromFile('Personal_data', 'C:\Users\17079\Desktop\myrepo\analytics\datasets\Personal_Data_Mini.tsv', '	');
CALL importFromFile('Cards', 'C:\Users\17079\Desktop\myrepo\analytics\datasets\Cards_Mini.tsv', '	');
CALL importFromFile('Transactions', 'C:\Users\17079\Desktop\myrepo\analytics\datasets\Transactions_Mini.tsv', '	');
CALL importFromFile('Checks', 'C:\Users\17079\Desktop\myrepo\analytics\datasets\Checks_Mini.tsv', '	');
CALL importFromFile('SKU', 'C:\Users\17079\Desktop\myrepo\analytics\datasets\SKU_Mini.tsv', '	');
CALL importFromFile('Stores', 'C:\Users\17079\Desktop\myrepo\analytics\datasets\Stores_Mini.tsv', '	');
CALL importFromFile('Groups_SKU', 'C:\Users\17079\Desktop\myrepo\analytics\datasets\Groups_SKU_Mini.tsv', '	');
CALL importFromFile('Date_Of_Analysis_Formation',
                    'C:\Users\17079\Desktop\myrepo\analytics\datasets\Date_Of_Analysis_Formation.tsv', '	');
-- --UNIX
-- CALL importFromFile('Personal_data', '/tmp/datasets/Personal_Data_Mini.tsv','	');
-- CALL importFromFile('Cards', '/tmp/datasets/Cards_Mini.tsv','	');
-- CALL importFromFile('Transactions','/tmp/datasets/Transactions_Mini.tsv','	');
-- CALL importFromFile('Checks', '/tmp/datasets/Checks_Mini.tsv', '	');
-- CALL importFromFile('SKU', '/tmp/datasets/SKU_Mini.tsv','	');
-- CALL importFromFile('Stores', '/tmp/datasets/Stores_Mini.tsv', '	');
-- CALL importFromFile('Groups_SKU','/tmp/datasets/Groups_SKU_Mini.tsv','	');
-- CALL importFromFile('Date_Of_Analysis_Formation','/tmp/datasets/Date_Of_Analysis_Formation.tsv','	');
--EXPORT UNIX
-- CALL exportToFile('Personal_data','/tmp/datasets/personal_data.csv',',');
-- CALL exportToFile('Cards', '/tmp/datasets/cards.csv', ',');
-- CALL exportToFile('Transactions','/tmp/datasets/transactions.csv',',');
-- CALL exportToFile('Checks', '/tmp/datasets/checks.csv', ',');
-- CALL exportToFile('SKU', '/tmp/datasets/sku.csv', ',');
-- CALL exportToFile('Stores', '/tmp/datasets/stores.csv', ',');
-- CALL exportToFile('Groups_SKU','/tmp/datasets/groups_sku.csv',',');
-- CALL exportToFile('Date_Of_Analysis_Formation','/tmp/datasets/date_of_analysis_formation.csv',' ');
-- REVOKE ALL ON ALL TABLES IN SCHEMA public FROM administrator, visitor;
-- REVOKE ALL ON SCHEMA public FROM administrator, visitor;
DROP ROLE IF EXISTS administrator, visitor;

CREATE ROLE administrator;
GRANT ALL ON SCHEMA public TO administrator;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA PUBLIC TO administrator;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA PUBLIC TO administrator;

CREATE ROLE visitor;
GRANT USAGE ON SCHEMA public TO visitor;
GRANT SELECT ON ALL TABLES IN SCHEMA PUBLIC TO visitor;

-- ADMIN TEST
SELECT *
FROM pg_roles pg
WHERE pg.rolname NOT SIMILAR TO 'pg{1}_+';
SET ROLE administrator;
SELECT current_user;

SELECT *
FROM personal_data
ORDER BY customer_ID
LIMIT 10;

INSERT INTO personal_data(customer_id, customer_name, customer_surname, customer_primary_email, customer_primary_phone)
VALUES (1001, 'Енакентий', 'Попугаевич', 'kukareku@ya.ru', '+79134040444');

SELECT *
FROM personal_data pd
WHERE pd.customer_id = 1001;

DELETE
FROM personal_data pd
WHERE pd.customer_name = 'Енакентий'
  AND pd.customer_surname = 'Попугаевич';

-- VISITOR TEST
SET ROLE visitor;
SELECT current_user;

SELECT *
FROM personal_data
ORDER BY customer_ID
LIMIT 10;

INSERT INTO personal_data(customer_id, customer_name, customer_surname, customer_primary_email, customer_primary_phone)
VALUES (1002, 'Енакентий', 'Попугаевич', 'kukareku@ya.ru', '+79134040444');

DELETE
FROM personal_data pd
WHERE pd.customer_id = 1000;

RESET ROLE;


DROP FUNCTION IF EXISTS formation_of_personal_offers_v2;
CREATE OR REPLACE FUNCTION formation_of_personal_offers_v2(groups_count INTEGER,
                                                           max_churn_index NUMERIC,
                                                           max_stability_Index NUMERIC,
                                                           max_sku_share NUMERIC,
                                                           allowable_margin_share NUMERIC)
    RETURNS TABLE
            (
                "Customer_ID"          BIGINT,
                "SKU_Name"             VARCHAR,
                "Offer_Discount_Depth" INT
            )
    LANGUAGE SQL
AS
$$
WITH group_selection AS (
    SELECT gr."Customer_ID",
           gr."Group_ID",
           gr."Group_Affinity_Index",
           gr."Group_Stability_Index",
           ROW_NUMBER() OVER (
               PARTITION BY gr."Customer_ID"
               ORDER BY gr."Customer_ID" ASC,
                   gr."Group_Affinity_Index" DESC
               ) AS r_num
    FROM groups gr
    WHERE gr."Group_Affinity_Index" <= max_churn_index
      AND gr."Group_Stability_Index" < max_stability_Index
),
     sku_group_id AS (
         SELECT DISTINCT sku.sku_id
         FROM sku
                  LEFT JOIN group_selection gs ON sku.group_id = gs."Group_ID"
     ),
     sku_max_margin AS (
         SELECT DISTINCT st.sku_id,
                         MAX(st.sku_retail_price - st.sku_purchase_price) OVER (Partition BY st.sku_id) AS max_margin
         FROM stores st
                  INNER JOIN sku_group_id sgi ON st.sku_id = sgi.sku_id
     ),
     sku_top AS (
         SELECT smm.sku_id,
                ROW_NUMBER() OVER (
                    PARTITION BY smm.sku_id
                    ORDER BY smm.sku_id ASC,
                        smm.max_margin DESC
                    ) AS r_num
         FROM sku_max_margin smm
     ),
     sku_name_res AS (
         SELECT DISTINCT sku.sku_name AS name,
                         sku.group_id AS id
         FROM sku
                  RIGHT JOIN sku_top st ON st.sku_id = sku.sku_id
             AND st.r_num = 1
     ),
     sku_c AS (
         SELECT DISTINCT c.sku_id,
                         COUNT(*) OVER (PARTITION BY c.sku_id) AS number_trans_in_sku
         FROM checks c
     ),
     group_c AS (
         SELECT DISTINCT group_id,
                         COUNT(*) OVER (PARTITION BY group_id) AS number_trans_in_group
         FROM checks c
                  JOIN sku s ON c.sku_id = s.sku_id
     ),
     need_sku AS (
         SELECT c.*,
                t.group_id,
                t.number_trans_in_group,
                (
                    number_trans_in_sku * 1.0 / number_trans_in_group
                    ) * 100 as share_group
         FROM (
                  SELECT s.sku_id,
                         g.*
                  FROM sku s
                           RIGHT JOIN group_c g ON s.group_id = g.group_id
              ) t
                  RIGHT JOIN sku_c c ON t.sku_id = c.sku_id
         WHERE (
                   number_trans_in_sku * 1.0 / number_trans_in_group
                   ) * 100 <= max_sku_share
     ),
     temp1 as (
         SELECT transaction_store_id,
                s.sku_id,
                allowable_margin_share * (SKU_Retail_Price - SKU_Purchase_Price) / SKU_Purchase_Price share
         FROM stores s
     ),
     min_dis AS (
         SELECT g."Customer_ID",
                g."Group_ID",
                g."Group_Minimum_Discount" * 100 Group_Minimum_Discount
         FROM groups g
     ),
     tmp_sk_with_g as (
         SELECT t.*,
                s.group_id,
                s.sku_name
         FROM sku s
                  RIGHT JOIN temp1 t ON s.sku_id = t.sku_id
     ),
     discount_calculation AS (
         SELECT "Customer_ID",
                sku_name,
                t Offer_Discount_Depth
         FROM (
                  SELECT m."Customer_ID",
                         sku_name,
                         group_minimum_discount,
                         CEILING((FLOOR(group_minimum_discount) / 5)) * 5 t,
                         share,
                         transaction_store_id
                  FROM min_dis m
                           JOIN tmp_sk_with_g tm ON m."Group_ID" = tm.group_id
              ) tmp
         WHERE t < share
           AND t > 0
     )
SELECT t."Customer_ID",
       t.name,
       MAX(d.Offer_Discount_Depth)
FROM (
         SELECT t."Customer_ID",
                "Group_ID",
                t.name
         FROM (
                  SELECT DISTINCT "Customer_ID",
                                  "Group_ID",
                                  snr.name
                  FROM group_selection
                           RIGHT JOIN sku_name_res snr ON snr.id = group_selection."Group_ID"
                  WHERE r_num <= groups_count
              ) t
                  INNER JOIN need_sku n ON t."Group_ID" = n.group_id
     ) t
         INNER JOIN discount_calculation d ON t."Customer_ID" = d."Customer_ID"
    AND t.name = d.sku_name
GROUP BY 1,
         2;
$$;
SELECT *
FROM formation_of_personal_offers_v2(5, 3, 0.5, 100, 30);
SELECT *
FROM formation_of_personal_offers_v2(2, 5, 0.8, 70, 50);
SELECT *
FROM formation_of_personal_offers_v2(3, 2, 1, 50, 50);
SELECT *
FROM formation_of_personal_offers_v2(8, 3, 1, 90, 80);
SELECT *
FROM formation_of_personal_offers_v2(7, 4, 0.2, 40, 30);
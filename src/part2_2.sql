DROP MATERIALIZED VIEW IF EXISTS history_purchases CASCADE;
CREATE MATERIALIZED VIEW history_purchases AS
(
SELECT t.customer_ID,
       t.Transaction_ID,
       t.transaction_datetime,
       t.group_id,
       s.sku_purchase_price * t.sku_amount Group_Cost,
       t.sku_summ                          Group_Summ,
       t.sku_summ_paid                     Group_Summ_Paid
FROM (
         SELECT customer_ID,
                t.Transaction_ID,
                t.transaction_datetime,
                t.transaction_store_id,
                ch.sku_id,
                ch.sku_amount,
                ch.sku_summ,
                ch.sku_summ_paid,
                group_id
         FROM date_of_analysis_formation,
              transactions t
                  JOIN cards c ON t.customer_card_id = c.customer_Card_ID
                  JOIN checks ch ON t.transaction_id = ch.transaction_id
                  JOIN SKU s ON ch.sku_id = s.sku_id
         WHERE transaction_datetime <= analysis_formation
     ) t
         JOIN stores s ON t.sku_id = s.sku_id
    AND t.transaction_store_id = s.transaction_store_id
ORDER BY 1,
         2,
         3
    );
SELECT *
FROM history_purchases
WHERE customer_id = 5;

SELECT *
FROM history_purchases
WHERE group_cost > 500;

SELECT *
FROM history_purchases

SELECT *
FROM history_purchases
WHERE group_summ = group_summ_paid;

SELECT *
FROM history_purchases
WHERE customer_id = 3
  AND group_id = 1;
-- определение группы для формирования вознаграждения
DROP FUNCTION IF EXISTS group_def(BIGINT, INT, NUMERIC, NUMERIC) CASCADE;
DROP FUNCTION IF EXISTS Formation_Of_Personal_Offers(TIMESTAMP, TIMESTAMP, NUMERIC, INT, NUMERIC, NUMERIC) CASCADE;
DROP FUNCTION IF EXISTS group_table(BIGINT) CASCADE;
DROP FUNCTION IF EXISTS group_margin(BIGINT, BIGINT) CASCADE;
CREATE OR REPLACE FUNCTION group_def(customer_id BIGINT,
                                     max_churn_index INT,
                                     max_share_disc NUMERIC,
                                     margin_value NUMERIC) RETURNS VARCHAR
    LANGUAGE plpgsql AS
$$
DECLARE
    counter    INT := 1;
    tmp_gid    BIGINT;
    tmp_gai    NUMERIC;
    tmp_gcr    NUMERIC;
    tmp_gad    NUMERIC;
    tmp_gds    NUMERIC;
    tmp_gmd    NUMERIC;
    tmp_gcount INT;
BEGIN
    SELECT COUNT(*)
    INTO tmp_gcount
    FROM group_table(customer_id) gt;
    LOOP
        SELECT gt.gid
        INTO tmp_gid
        FROM group_table(customer_id) gt
        WHERE row_number = counter;
        SELECT gt.gai
        INTO tmp_gai
        FROM group_table(customer_id) gt
        WHERE row_number = counter;
        SELECT gt.gcr
        INTO tmp_gcr
        FROM group_table(customer_id) gt
        WHERE row_number = counter;
        SELECT gt.gad
        INTO tmp_gad
        FROM group_table(customer_id) gt
        WHERE row_number = counter;
        SELECT gt.gds
        INTO tmp_gds
        FROM group_table(customer_id) gt
        WHERE row_number = counter;
        SELECT gt.gmd
        INTO tmp_gmd
        FROM group_table(customer_id) gt
        WHERE row_number = counter;
        counter := counter + 1;
        IF (
                (tmp_gcr <= max_churn_index)
                AND (tmp_gds <= max_share_disc / 100::NUMERIC)
                AND (
                        margin_value / 100::NUMERIC * group_margin(customer_id, tmp_gid) > CEILING(20 * tmp_gmd) / 20::NUMERIC
                    )
                AND tmp_gmd > 0
            ) THEN
            EXIT;
        END IF;
        IF (counter > tmp_gcount) THEN
            RETURN (NULL);
            EXIT;
        END IF;
    END LOOP;
    RETURN (
        SELECT gs.group_name
        FROM groups_sku gs
        WHERE gs.group_id = tmp_gid
    );
END;
$$;
CREATE OR REPLACE FUNCTION Formation_Of_Personal_Offers(first_date TIMESTAMP,
                                                        last_date TIMESTAMP,
                                                        num_of_transactions NUMERIC,
                                                        max_churn_index INT,
                                                        max_share_disc NUMERIC,
                                                        margin_value NUMERIC)
    RETURNS TABLE
            (
                "Customer_ID"                 BIGINT,
                "Start_Date"                  TIMESTAMP,
                "End_Date"                    TIMESTAMP,
                "Required_Transactions_Count" NUMERIC,
                "Group_Name"                  VARCHAR,
                "Offer_Discount_Depth"        NUMERIC
            )
    LANGUAGE SQL
AS
$$
WITH base_transaction_intensity AS (
    SELECT c.customer_id,
           ROUND(
                           (
                                   EXTRACT(
                                           DAYS
                                           FROM (first_date - last_date)
                                       ) + EXTRACT(
                                                   HOURS
                                                   FROM (first_date - last_date)
                                               ) / 24::numeric + EXTRACT(
                                                                         MINUTES
                                                                         FROM (first_date - last_date)
                                                                     ) / 1440::numeric + EXTRACT(
                                                                                                 SECONDS
                                                                                                 FROM
                                                                                                 (first_date - last_date)
                                                                                             ) / 86400::numeric
                               ) / c.customer_frequency + num_of_transactions
               ) AS Required_Transactions_Count
    FROM clients c
    GROUP BY c.customer_id,
             c.customer_frequency
),
     result_ AS (
         SELECT bti.customer_id                 AS "Customer_ID",
                first_date                      AS "Start_Date",
                last_date                       AS "End_Date",
                bti.Required_Transactions_Count AS "Required_Transactions_Count",
                group_def(
                        bti.customer_id,
                        max_churn_index,
                        max_share_disc,
                        margin_value
                    )                           AS "Group_Name"
         FROM base_transaction_intensity AS bti
         ORDER BY customer_id
     )
SELECT DISTINCT r."Customer_ID",
                r."Start_Date",
                r."End_Date",
                r."Required_Transactions_Count",
                r."Group_Name",
                (
                    CEILING(20 * gr."Group_Minimum_Discount") / 20::NUMERIC
                    ) * 100 AS "Offer_Discount_Depth"
FROM result_ r
         LEFT JOIN groups_sku gs ON r."Group_Name" = gs.group_name
         LEFT JOIN groups gr ON gr."Group_ID" = gs.group_id
    AND r."Customer_ID" = gr."Customer_ID"
WHERE "Group_Name" IS NOT NULL
ORDER BY r."Customer_ID";
$$;
-- Возврат значений из таблицы по customer_id отсортированной по убыванию с наличием счётчика строк
CREATE OR REPLACE FUNCTION group_table(customer_id BIGINT)
    RETURNS TABLE
            (
                gid        BIGINT,
                gai        NUMERIC,
                gcr        NUMERIC,
                gad        NUMERIC,
                gds        NUMERIC,
                gmd        NUMERIC,
                row_number BIGINT
            )
    LANGUAGE plpgsql
AS
$$
BEGIN
    RETURN QUERY (
        SELECT g."Group_ID"               AS gid,
               g."Group_Affinity_Index"   AS gai,
               g."Group_Churn_Rate"       AS gcr,
               g."Group_Average_Discount" AS gad,
               g."Group_Discount_Share"   AS gds,
               g."Group_Minimum_Discount" AS gmd,
               ROW_NUMBER() OVER (
                   ORDER BY g."Customer_ID" ASC,
                       g."Group_Affinity_Index" DESC
                   )
        FROM groups g
        WHERE g."Customer_ID" = customer_id
    );
END;
$$;
-- определение доли маржи
CREATE OR REPLACE FUNCTION group_margin(customer_id_f BIGINT, group_id_f BIGINT) RETURNS NUMERIC
    LANGUAGE plpgsql AS
$$
BEGIN
    RETURN (
        SELECT DISTINCT AVG(
                        (hp.group_summ_paid - hp.group_cost) / hp.group_summ_paid
                            ) OVER (PARTITION BY hp.customer_id, hp.group_id)
        FROM history_purchases hp
        WHERE hp.customer_id = customer_id_f
          AND hp.group_id = group_id_f
    );
END;
$$;
-- Проверка
SELECT *
FROM Formation_Of_Personal_Offers('2021-08-18', '2022-08-18', 10, 2, 70, 30);
SELECT *
FROM Formation_Of_Personal_Offers('2018-08-18', '2022-08-18', 1, 3, 70, 30);
SELECT *
FROM Formation_Of_Personal_Offers('2018-08-18', '2022-08-18', 8, 10, 70, 30);
SELECT *
FROM Formation_Of_Personal_Offers('2018-08-18', '2022-08-18', 1, 3, 80, 20);
SELECT *
FROM Formation_Of_Personal_Offers('2022-08-18', '2022-08-18', 1, 3, 70, 30);
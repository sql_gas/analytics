DROP MATERIALIZED VIEW IF EXISTS periods CASCADE;
CREATE MATERIALIZED VIEW periods AS
(
WITH temp AS (
    SELECT customer_id,
           sk.group_id,
           transaction_datetime,
           sku_discount,
           sku_discount / sku_summ AS Min_Discount
    FROM transactions t
             JOIN cards c on c.customer_card_id = t.customer_card_id
             JOIN checks ch on t.transaction_id = ch.transaction_id
             JOIN sku sk on ch.sku_id = sk.sku_id
             LEFT JOIN stores s on sk.sku_id = s.sku_id
        AND s.transaction_store_id = t.transaction_store_id
    WHERE transaction_datetime <= (
        SELECT MAX(analysis_formation)
        FROM date_of_analysis_formation
    )
),
     min_diskount AS (
         SELECT customer_id,
                group_id,
                COALESCE(MIN(Min_Discount), 0) AS Group_Min_Discount
         FROM temp
         WHERE sku_discount != 0
         GROUP BY customer_id,
                  group_id
     )
SELECT DISTINCT h.customer_ID,
                h.group_id,
                MIN(Transaction_DateTime)          First_Group_Purchase_Date,
                MAX(Transaction_DateTime)          Last_Group_Purchase_Date,
                COUNT(h.Transaction_ID)            Group_Purchase,
                (
                            EXTRACT(
                                    epoch
                                    FROM MAX(Transaction_DateTime) - MIN(Transaction_DateTime)
                                ) / 86400 + 1
                    ) / COUNT(h.Transaction_ID) AS Group_Frequency,
                m.Group_Min_Discount
FROM history_purchases h
         JOIN checks ch ON h.transaction_id = ch.transaction_id
         JOIN min_diskount m ON h.customer_id = m.customer_id
    AND h.group_id = m.group_id
GROUP BY h.customer_ID,
         h.group_id,
         m.Group_Min_Discount
ORDER BY h.customer_ID,
         h.group_id
    );
SELECT *
FROM periods
WHERE customer_id = 20;

SELECT *
FROM periods
WHERE First_Group_Purchase_Date > '01-01-2021';

SELECT *
FROM periods;

SELECT *
FROM periods
WHERE group_purchase > 8;

SELECT *
FROM periods
WHERE group_min_discount > 0.4;

DROP FUNCTION IF EXISTS primary_store(BIGINT) CASCADE;
DROP MATERIALIZED VIEW IF EXISTS Clients CASCADE;
CREATE OR REPLACE FUNCTION primary_store(customer_id_f BIGINT) RETURNS BIGINT
    LANGUAGE sql AS
$$
WITH all_visits AS (
    SELECT c.customer_id,
           transactions.transaction_store_id AS id,
           transactions.transaction_datetime
    FROM date_of_analysis_formation,
         transactions
             JOIN cards c ON c.customer_card_id = transactions.customer_card_id
    WHERE transaction_datetime <= analysis_formation
      AND c.customer_id = customer_id_f
    ORDER BY customer_id ASC,
             transactions.transaction_datetime DESC
),
     last_three_visits AS (
         SELECT *
         FROM (
                  SELECT ROW_NUMBER() OVER (PARTITION BY customer_id) AS rank,
                         av.*
                  FROM all_visits av
              ) avf
         WHERE avf.rank <= 3
     ),
     most_visits AS (
         SELECT DISTINCT t.transaction_store_id      AS store_id,
                         COUNT(t.transaction_id)     AS count_transactions,
                         MAX(t.transaction_datetime) AS visit_date
         FROM transactions t
         WHERE t.customer_card_id IN (
             SELECT customer_card_id
             FROM cards
             WHERE customer_id = customer_id_f
         )
           AND t.transaction_datetime < (
             SELECT analysis_formation
             FROM date_of_analysis_formation
             LIMIT 1
         )
         GROUP BY transaction_store_id
     )
SELECT CASE
           WHEN (
                    SELECT AVG(id)
                    FROM last_three_visits
                    WHERE customer_id = customer_id_f
                ) = (
                    SELECT MAX(id)
                    FROM last_three_visits
                    WHERE customer_id = customer_id_f
                )
               AND (
                       SELECT MAX(id)
                       FROM last_three_visits
                       WHERE customer_id = customer_id_f
                   ) = (
                       SELECT MIN(id)
                       FROM last_three_visits
                       WHERE customer_id = customer_id_f
                   ) THEN (
               SELECT MAX(id)
               FROM last_three_visits
               WHERE customer_id = customer_id_f
           )
           ELSE (
               SELECT mv.store_id
               FROM most_visits mv
               WHERE mv.count_transactions = (
                   SELECT MAX(count_transactions)
                   FROM most_visits
               )
               ORDER BY mv.visit_date DESC
               LIMIT 1
           )
           END AS primary_store_id;
$$;

CREATE MATERIALIZED VIEW Clients AS
WITH count_client AS (
    SELECT COUNT(*) c
    FROM (
             SELECT DISTINCT customer_id
             FROM cards
         ) t
),
     temp_check AS (
         SELECT customer_id,
                customer_average_check,
                (
                    CASE
                        WHEN row_number() OVER () <= c * 0.1 THEN 'High'
                        WHEN row_number() OVER () > c * 0.1
                            AND row_number() OVER () <= c * 0.35 THEN 'Medium'
                        ELSE 'Low'
                        END
                    ) customer_average_check_segment,
                (
                    CASE
                        WHEN row_number() OVER () <= c * 0.1 THEN 18
                        WHEN row_number() OVER () > c * 0.1
                            AND row_number() OVER () <= c * 0.35 THEN 9
                        ELSE 0
                        END
                    ) ch
         FROM count_client,
              (
                  SELECT customer_ID,
                         (SUM(transaction_summ) / COUNT(*)) customer_average_check
                  FROM date_of_analysis_formation,
                       cards c
                           JOIN transactions t ON c.customer_Card_ID = t.customer_card_id
                  WHERE transaction_datetime <= analysis_formation
                  GROUP BY customer_id
                  ORDER BY customer_average_check DESC
              ) t
     ),
     temp_frequency AS (
         SELECT customer_id,
                customer_frequency,
                (
                    CASE
                        WHEN row_number() OVER () <= c * 0.1 THEN 'Often'
                        WHEN row_number() OVER () > c * 0.1
                            AND row_number() OVER () <= c * 0.35 THEN 'Occasionally'
                        ELSE 'Rarely'
                        END
                    )                                         customer_frequency_segment,
                (
                    CASE
                        WHEN row_number() OVER () <= c * 0.1 THEN 6
                        WHEN row_number() OVER () > c * 0.1
                            AND row_number() OVER () <= c * 0.35 THEN 3
                        ELSE 0
                        END
                    )                                         fr,
                customer_inactive_period,
                customer_inactive_period / customer_frequency customer_churn_rate,
                (
                    CASE
                        WHEN customer_inactive_period / customer_frequency <= 2 THEN 'Low'
                        WHEN customer_inactive_period / customer_frequency > 2
                            AND customer_inactive_period / customer_frequency <= 5 THEN 'Medium'
                        ELSE 'High'
                        END
                    )                                         customer_churn_segment,
                (
                    CASE
                        WHEN customer_inactive_period / customer_frequency <= 2 THEN 1
                        WHEN customer_inactive_period / customer_frequency > 2
                            AND customer_inactive_period / customer_frequency <= 5 THEN 2
                        ELSE 3
                        END
                    )                                         cs
         FROM count_client,
              (
                  SELECT c.customer_id,
                         (
                                 MAX(t.transaction_datetime)::date - MIN(t.transaction_datetime)::date
                             ) / COUNT(t.transaction_datetime)::NUMERIC customer_frequency,
                         (
                                 EXTRACT(
                                         EPOCH
                                         FROM (MAX(analysis_formation))
                                     ) - EXTRACT(
                                         EPOCH
                                         FROM (MAX(t.transaction_datetime))
                                     )
                             ) / 86000                                  customer_inactive_period
                  FROM date_of_analysis_formation,
                       transactions t
                           JOIN cards c on c.customer_card_id = t.customer_card_id
                  WHERE transaction_datetime <= analysis_formation
                  GROUP BY c.customer_id
                  ORDER BY customer_frequency
              ) t
     )
SELECT c.customer_id,
       c.customer_average_check,
       c.customer_average_check_segment,
       f.customer_frequency,
       f.customer_frequency_segment,
       f.customer_inactive_period,
       f.customer_churn_rate,
       f.customer_churn_segment,
       (fr + cs + ch)                  customer_segment,
       primary_store(c.customer_id) AS customer_primary_store
FROM temp_frequency f
         JOIN temp_check c ON f.customer_id = c.customer_id --Расчет доли транзакций в каждом магазине
ORDER BY c.customer_id ASC;

SELECT *
FROM clients;

SELECT *
FROM clients
WHERE customer_primary_store = 3;

SELECT *
FROM clients
WHERE customer_frequency_segment = 'Occasionally';

SELECT *
FROM clients
WHERE customer_average_check > 1100;
-- DROP FUNCTION fn_choose_group_period CASCADE;
CREATE OR REPLACE FUNCTION fn_choose_group_period(first_date DATE, last_date DATE)
    RETURNS TABLE
            (
                "Customer_ID"            BIGINT,
                "Required_Check_Measure" NUMERIC
            )
AS
' BEGIN
    RETURN QUERY
        SELECT c.customer_ID,
               ((SUM(transaction_summ) / COUNT(*))) Required_Check_Measure
        FROM date_of_analysis_formation,
             cards c
                 JOIN transactions t ON c.customer_Card_ID = t.customer_card_id
        WHERE transaction_datetime <= last_date
          AND transaction_datetime >= first_date
        GROUP BY c.customer_id
        ORDER BY c.customer_id;
END ' LANGUAGE plpgsql;
--
-- DROP FUNCTION fn_choose_group_count CASCADE;
CREATE OR REPLACE FUNCTION fn_choose_group_count(count_transaction INT)
    RETURNS TABLE
            (
                "Customer_ID"            BIGINT,
                "Required_Check_Measure" NUMERIC
            )
AS
$$
BEGIN
    RETURN QUERY WITH tmp AS (
        SELECT c.customer_id,
               transaction_summ,
               transaction_datetime
        FROM date_of_analysis_formation,
             transactions t
                 JOIN cards c on c.customer_card_id = t.customer_card_id
        WHERE transaction_datetime <= analysis_formation
        ORDER BY c.customer_id,
                 transaction_datetime DESC
    )
                 SELECT t.customer_id,
                        (SUM(t.transaction_summ) / MAX(r)) Required_Check_Measure
                 FROM (
                          SELECT *
                          FROM (
                                   SELECT ROW_NUMBER() OVER (PARTITION BY t.customer_id) AS r,
                                          t.*
                                   FROM tmp t
                               ) x
                          WHERE x.r <= 100
                      ) t
                 GROUP BY t.customer_id;
END
$$ LANGUAGE plpgsql;
--
-- DROP FUNCTION IF EXISTS fn_temp;
CREATE OR REPLACE FUNCTION fn_temp(method INT,
                                   first_date DATE,
                                   last_date DATE,
                                   count_transaction INT)
    RETURNS TABLE
            (
                "Customer_ID"            BIGINT,
                "Required_Check_Measure" NUMERIC
            )
AS
$$
BEGIN
    IF method = 1 THEN
        RETURN QUERY (
            SELECT *
            FROM fn_choose_group_period(first_date, last_date)
        );
    ELSE
        RETURN QUERY (
            SELECT *
            FROM fn_choose_group_count(count_transaction)
        );
    END IF;
END;
$$ LANGUAGE plpgsql;
--
--
DROP FUNCTION IF EXISTS fn_offer_discount CASCADE;
CREATE OR REPLACE FUNCTION fn_offer_discount(Customer_ID BIGINT,
                                             max_index DECIMAL,
                                             max_discounted BIGINT[]) RETURNS BIGINT[] AS
$$
DECLARE
    v_minimal_discount DECIMAL = 1;
    v_maximal_discount DECIMAL = 0;
    v_result           BIGINT[];
BEGIN
    WHILE v_minimal_discount > v_maximal_discount
        LOOP
            SELECT "Group_Average_Discount" * (max_index)
            FROM groups g
            WHERE g."Group_ID" = max_discounted[1]
              AND customer_id = Customer_ID
            INTO v_maximal_discount;
            SELECT CEILING((FLOOR("Group_Minimum_Discount" * 100) / 5)) * 5
            FROM groups g
            WHERE g."Group_ID" = max_discounted[2]
              AND customer_id = Customer_ID
            INTO v_minimal_discount;
            IF v_minimal_discount > v_maximal_discount THEN
                max_discounted = array_remove(max_discounted, max_discounted[1]);
            END IF;
        END LOOP;
    v_result := array_append(v_result, v_minimal_discount);
    v_result := array_append(v_result, max_discounted[2]);
    RETURN v_result;
END;
$$ LANGUAGE plpgsql;
--
--
-- DROP FUNCTION IF EXISTS fn_offer_group_list CASCADE;
CREATE OR REPLACE FUNCTION fn_offer_group_list(Customer_id BIGINT,
                                               max_index DECIMAL,
                                               max_discounted DECIMAL) RETURNS BIGINT[] AS
$$
DECLARE
    group_affinity_list BIGINT[];
BEGIN
    WITH groups_tmp AS (
        SELECT g."Group_ID",
               "Group_Affinity_Index"
        FROM groups g
        WHERE Customer_ID = g."Customer_ID"
          AND "Group_Churn_Rate" < max_index
          AND "Group_Discount_Share" < max_discounted / 100::DECIMAL
        GROUP BY "Group_Affinity_Index",
                 g."Group_ID"
        ORDER BY "Group_Affinity_Index" DESC
    )
    SELECT array_agg("Group_ID")
    INTO group_affinity_list
    FROM groups_tmp;
    RETURN group_affinity_list;
END;
$$ LANGUAGE plpgsql;
--
--
-- DROP FUNCTION IF EXISTS personal_tmp CASCADE;
CREATE OR REPLACE FUNCTION personal_tmp(max_index NUMERIC,
                                        max_discounted NUMERIC,
                                        margin NUMERIC)
    RETURNS TABLE
            (
                "Customer_ID"          BIGINT,
                "Group_Name"           TEXT,
                "Offer_Discount_Depth" NUMERIC
            )
AS
$$
DECLARE
    v_customer_id      BIGINT;
    v_offer_group_list BIGINT[];
    v_offer_group      TEXT;
    v_offer_discount   BIGINT[];
BEGIN
    CREATE TEMP TABLE t_result
    (
        "Customer_ID"          BIGINT,
        "Group_Name"           TEXT,
        "Offer_Discount_Depth" NUMERIC
    );
    FOR v_customer_id IN
        SELECT c."customer_id"
        FROM Clients c
        LOOP
            v_offer_group_list := fn_offer_group_list(v_customer_id, max_index, max_discounted);
            v_offer_discount := fn_offer_discount(v_customer_id, margin, v_offer_group_list);
            v_offer_group := (
                SELECT group_name
                FROM groups_sku
                WHERE Group_ID = v_offer_discount[2]
            );
            INSERT INTO t_result
            SELECT v_customer_id,
                   v_offer_group,
                   v_offer_discount[1];
        END LOOP;
    RETURN QUERY
        SELECT *
        FROM t_result;
    DROP TABLE t_result;
END
$$ LANGUAGE plpgsql;
--
-- DROP FUNCTION IF EXISTS fn_personal_offers;
CREATE OR REPLACE FUNCTION fn_personal_offers(method INT = 2,
                                              first_date DATE = NULL,
                                              last_date DATE = NULL,
                                              count_transaction INT = 100,
                                              c_check NUMERIC = 1.15,
                                              max_index NUMERIC = 3,
                                              max_discounted NUMERIC = 70,
                                              margin NUMERIC = 30)
    RETURNS TABLE
            (
                "Customer_ID"            BIGINT,
                "Required_Check_Measure" NUMERIC,
                "Group_Name"             TEXT,
                "Offer_Discount_Depth"   NUMERIC
            )
AS
$$
BEGIN
    RETURN QUERY WITH tmp AS (
        SELECT *
        FROM fn_temp(
                method,
                first_date,
                last_date,
                count_transaction
            )
    ),
                      tmp_offers AS (
                          SELECT *
                          FROM personal_tmp(
                                  max_index,
                                  max_discounted,
                                  margin
                              )
                      )
                 SELECT t1."Customer_ID",
                        t1."Required_Check_Measure" * c_check,
                        t2."Group_Name",
                        t2."Offer_Discount_Depth"
                 FROM tmp t1
                          JOIN tmp_offers t2 ON t1."Customer_ID" = t2."Customer_ID"
                 WHERE t2."Group_Name" IS NOT NULL;
END
$$ LANGUAGE plpgsql;
SELECT *
FROM fn_personal_offers(
        1,
        '10-10-2019',
        '10-10-2021',
        null,
        1.05,
        4,
        60,
        20
    );
SELECT *
FROM fn_personal_offers()
WHERE "Customer_ID" = 3;
SELECT *
FROM fn_personal_offers(
        2,
        '10-10-2019',
        '10-10-2021',
        null,
        1.7,
        5,
        70,
        10
    );
SELECT *
FROM fn_personal_offers(
        12,
        null,
        null,
        50,
        1.7,
        5,
        70,
        10
    );
SELECT *
FROM fn_personal_offers(
        2,
        null,
        null,
        100,
        1.15,
        3,
        70,
        40
    )
DROP FUNCTION IF EXISTS fn_grp CASCADE;
CREATE OR REPLACE FUNCTION fn_grp(method INT = 1, _count INT = 100)
    RETURNS TABLE
            (
                "Customer_ID"            BIGINT,
                "Group_ID"               BIGINT,
                "Group_Affinity_Index"   NUMERIC,
                "Group_Churn_Rate"       NUMERIC,
                "Group_Stability_Index"  NUMERIC,
                "Group_Margin"           NUMERIC,
                "Group_Discount_Share"   NUMERIC,
                "Group_Minimum_Discount" NUMERIC,
                "Group_Average_Discount" NUMERIC
            )
AS
$$
BEGIN
    RETURN QUERY WITH temp_groups AS (
        SELECT c.customer_id,
               ch.sku_id
        FROM checks ch
                 JOIN transactions t on ch.transaction_id = t.transaction_id
                 JOIN cards c on t.customer_card_id = c.customer_card_id
                 JOIN sku s ON ch.sku_id = s.sku_id
    ),
                      uniq_groups AS (
                          SELECT DISTINCT g.customer_id,
                                          s.group_id
                          FROM temp_groups g
                                   JOIN sku s ON g.sku_id = s.sku_id
                      ),
                      g_affinity AS (
                          SELECT u.customer_id,
                                 u.group_id,
                                 group_purchase,
                                 COUNT(h.transaction_id),
                                 group_purchase::NUMERIC / COUNT(h.transaction_id)::NUMERIC AS Group_Affinity_Index
                          FROM uniq_groups u
                                   JOIN periods p ON u.group_id = p.group_id
                              AND u.customer_id = p.customer_id
                                   JOIN history_purchases h ON u.customer_id = h.customer_id
                              AND h.transaction_datetime BETWEEN first_group_purchase_date AND last_group_purchase_date
                          GROUP BY u.customer_id,
                                   u.group_id,
                                   group_purchase
                      ),
                      g_churn_rate AS (
                          SELECT u.customer_id,
                                 u.group_id,
                                 (
                                         EXTRACT(
                                                 epoch
                                                 FROM analysis_formation - last_group_purchase_date
                                             ) / 86400 / p.group_frequency
                                     ) AS Group_Churn_Rate
                          FROM date_of_analysis_formation,
                               uniq_groups u
                                   JOIN periods p ON u.group_id = p.group_id
                                   AND u.customer_id = p.customer_id
                      ),
                      intervals AS (
                          SELECT customer_id,
                                 group_id,
                                 transaction_datetime,
                                 (
                                         EXTRACT(
                                                 epoch
                                                 FROM transaction_datetime - LAG(transaction_datetime) OVER (
                                             PARTITION BY customer_id,
                                                 group_id
                                             ORDER BY transaction_datetime
                                             )
                                             ) / 86400
                                     ) AS days
                          FROM history_purchases
                      ),
                      g_stability_index AS (
                          SELECT i.customer_id,
                                 i.group_id,
                                 AVG(
                                         CASE
                                             WHEN (days - p.group_frequency) < 0
                                                 THEN ((days - p.group_frequency) * - 1) / group_frequency
                                             ELSE (days - p.group_frequency) / group_frequency
                                             END
                                     ) AS Group_Stability_Index
                          FROM intervals i
                                   JOIN periods p ON i.customer_id = p.customer_id
                              AND i.group_id = p.group_id
                          WHERE i.days IS NOT NULL
                          GROUP BY i.customer_id,
                                   i.group_id
                      ),
                      total_discount AS (
                          SELECT h.customer_id,
                                 h.group_id,
                                 COUNT(sku_discount) AS number_of_purchase
                          FROM history_purchases h
                                   JOIN checks c on h.transaction_id = c.transaction_id
                          WHERE sku_discount != 0
                          GROUP BY h.customer_id,
                                   h.group_id
                      ),
                      discount_share AS (
                          SELECT p.customer_id,
                                 p.group_id,
                                 (number_of_purchase / p.group_purchase::numeric) AS Group_Discount_Share
                          FROM total_discount t
                                   JOIN periods p ON t.customer_id = p.customer_id
                              AND t.group_id = p.group_id
                      ),
                      minimum_discount AS (
                          SELECT d.customer_id,
                                 d.group_id,
                                 MIN(p.group_min_discount) AS Group_Minimum_Discount
                          FROM discount_share d
                                   JOIN periods p ON d.customer_id = p.customer_id
                              AND d.group_id = p.group_id
                          GROUP BY d.customer_id,
                                   d.group_id
                      ),
                      average_discount AS (
                          SELECT customer_id,
                                 group_id,
                                 AVG(group_summ_paid::numeric / group_summ::numeric) AS Group_Average_Discount
                          FROM history_purchases
                          WHERE group_summ_paid != group_summ
                          GROUP BY customer_id,
                                   group_id
                      ),
                      margin AS (
                          SELECT customer_id,
                                 group_id,
                                 SUM(group_summ_paid) - SUM(group_cost) Group_Margin
                          FROM (
                                   SELECT customer_id,
                                          group_id,
                                          ROW_NUMBER() OVER (
                                              PARTITION BY customer_id,
                                                  group_id
                                              ORDER BY customer_id,
                                                  group_id,
                                                  transaction_datetime DESC
                                              ) AS      transaction_number,
                                          (
                                                  EXTRACT(
                                                          EPOCH
                                                          FROM (d.analysis_formation)
                                                      ) - EXTRACT(
                                                          EPOCH
                                                          FROM (transaction_datetime)
                                                      )
                                              ) / 86000 dayd,
                                          group_summ_paid,
                                          group_cost,
                                          transaction_datetime
                                   FROM history_purchases,
                                        date_of_analysis_formation d
                               ) t
                          WHERE CASE
                                    WHEN method = 1 THEN dayd <= _count
                                    ELSE transaction_number <= _count
                                    END
                          GROUP BY 1,
                                   2
                      )
                 SELECT a.customer_id,
                        a.group_id,
                        a.Group_Affinity_Index,
                        c.Group_Churn_Rate,
                        s.Group_Stability_Index,
                        n.Group_Margin,
                        d.Group_Discount_Share,
                        m.Group_Minimum_Discount,
                        v.Group_Average_Discount
                 FROM g_affinity a
                          JOIN g_churn_rate c ON a.customer_id = c.customer_id
                     AND a.group_id = c.group_id
                          JOIN g_stability_index s ON a.customer_id = s.customer_id
                     AND a.group_id = s.group_id
                          JOIN discount_share d ON a.customer_id = d.customer_id
                     AND a.group_id = d.group_id
                          JOIN minimum_discount m ON a.customer_id = m.customer_id
                     AND a.group_id = m.group_id
                          JOIN average_discount v ON a.customer_id = v.customer_id
                     AND a.group_id = v.group_id
                          JOIN margin n ON a.customer_id = n.customer_id
                     AND a.group_id = n.group_id;
END
$$ LANGUAGE plpgsql;

DROP MATERIALIZED VIEW IF EXISTS groups;
CREATE MATERIALIZED VIEW groups AS
SELECT *
FROM fn_grp(2, 100);

SELECT *
FROM groups;

SELECT *
FROM groups
WHERE "Group_Margin" > 500;

SELECT *
FROM groups
WHERE (
        "Customer_ID" = 1
        AND "Group_ID" = 7
    )
   OR (
        "Customer_ID" = 3
        AND "Group_ID" = 1
    );

DROP MATERIALIZED VIEW IF EXISTS groups;
CREATE MATERIALIZED VIEW groups AS

SELECT *
FROM fn_grp(1, 100);

SELECT *
FROM groups;

DROP MATERIALIZED VIEW IF EXISTS groups;
CREATE MATERIALIZED VIEW groups AS

SELECT *
FROM fn_grp(2, 15);